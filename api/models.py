# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class FirmwareVersion(models.Model):
    app = models.CharField(max_length=55)
    version = models.CharField(max_length=55)

    class Meta:
        db_table = 'firmware_version'


class XHcTesttblSensorId(models.Model):
    controller_id = models.IntegerField()
    hc_id = models.IntegerField()
    sensor_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = '_x_hc_testtbl_sensor_id'


class XSensorCurrentStatus(models.Model):
    controller_id = models.IntegerField()
    sensor_id = models.IntegerField()
    value = models.FloatField()

    class Meta:
        managed = False
        db_table = '_x_sensor_current_status'


class AppCronJobs(models.Model):
    job_name = models.CharField(primary_key=True, max_length=255)
    description = models.TextField(blank=True, null=True)
    enabled = models.IntegerField()
    last_run_result = models.TextField(blank=True, null=True)
    last_run_time = models.IntegerField(blank=True, null=True)
    job_interval = models.IntegerField(blank=True, null=True)
    job_start_time = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_cron_jobs'


class AppLogError(models.Model):
    time = models.IntegerField(blank=True, null=True)
    ip = models.CharField(max_length=100, blank=True, null=True)
    uri = models.CharField(max_length=100, blank=True, null=True)
    message = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'app_log_error'


class AppLogLogin(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=50)
    ip = models.CharField(max_length=100)
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'app_log_login'


class AppLogRest(models.Model):
    controller_sn = models.IntegerField(blank=True, null=True)
    uri = models.CharField(max_length=255)
    method = models.CharField(max_length=6)
    ip_address = models.CharField(max_length=45)
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'app_log_rest'


class AppLogUserActions(models.Model):
    user_id = models.IntegerField()
    username = models.TextField(blank=True, null=True)
    controller_id = models.IntegerField()
    action = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'app_log_user_actions'


class AppUserControllerAccess(models.Model):
    user_id = models.IntegerField(primary_key=True)
    controller_id = models.IntegerField(primary_key=True)
    date_created = models.IntegerField(blank=True, null=True)
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'app_user_controller_access'
        unique_together = (('user_id', 'controller_id'),)


class AppUserData(models.Model):
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=20, primary_key=True)
    password = models.TextField()
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50, primary_key=True)
    salt = models.CharField(max_length=255, blank=True, null=True)
    last_access = models.IntegerField(blank=True, null=True)
    date_created = models.IntegerField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'app_user_data'
        unique_together = (('user_id', 'email', 'username'),)


class AppUserDataExtra(models.Model):
    user_id = models.IntegerField()
    state = models.CharField(max_length=100, blank=True, null=True)
    zip = models.CharField(max_length=20, blank=True, null=True)
    city = models.CharField(max_length=50)
    country_id = models.IntegerField()
    language = models.CharField(max_length=11)
    date_created = models.IntegerField(blank=True, null=True)
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'app_user_data_extra'


class AppUserRoleData(models.Model):
    role_id = models.BigAutoField(primary_key=True)
    role_name = models.CharField(unique=True, max_length=20)
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'app_user_role_data'


class AppUserRoles(models.Model):
    user_id = models.BigIntegerField(primary_key=True)
    role_id = models.BigIntegerField()
    date_created = models.IntegerField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'app_user_roles'
        unique_together = (('user_id', 'role_id'),)


class Cities(models.Model):
    name = models.CharField(max_length=200)
    post_no = models.CharField(max_length=20, blank=True, null=True)
    country_id = models.IntegerField(blank=True, null=True)
    country_isocode = models.CharField(max_length=4, blank=True, null=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    geonames_id = models.IntegerField(blank=True, null=True)
    timezone = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cities'


class CitiesWeatherSubscription(models.Model):
    city = models.ForeignKey(Cities, models.DO_NOTHING)
    weather_provider = models.CharField(max_length=255)
    subscribed = models.IntegerField()
    provider_id = models.CharField(max_length=100, blank=True, null=True)
    last_weather_check = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cities_weather_subscription'


class Controller(models.Model):
    controller_id = models.AutoField(primary_key=True)
    serial_no = models.IntegerField()
    name = models.CharField(max_length=31)
    status = models.IntegerField()
    country_id = models.IntegerField()
    city_id = models.IntegerField()
    openweather_city_id = models.IntegerField(default=0)
    active_profile_id = models.IntegerField(default=0)
    last_report = models.IntegerField(default=0)
    last_change = models.IntegerField()
    date_created = models.IntegerField()
    timestamp = models.DateTimeField()
    auth_access_token = models.CharField(max_length=255, blank=True, null=True)
    auth_expires_at = models.IntegerField(default=0)
    auth_wildcard = models.SmallIntegerField(default=0)
    resque = models.SmallIntegerField(default=0)

    class Meta:
        managed = True
        db_table = 'controller'


class ControllerAcl(models.Model):
    type = models.CharField(max_length=15)
    controller_id = models.IntegerField()
    item_id = models.SmallIntegerField()
    zone_id = models.SmallIntegerField()
    acl = models.TextField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_acl'


class ControllerComment(models.Model):
    controller_id = models.IntegerField(blank=True, null=True)
    comment = models.CharField(max_length=255, blank=True, null=True)
    date_created = models.IntegerField(blank=True, null=True)
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'controller_comment'


class ControllerFileupload2(models.Model):
    file_id = models.IntegerField(primary_key=True)
    controller_id = models.IntegerField()
    filename = models.CharField(max_length=50, blank=True, null=True)
    filename_full = models.CharField(max_length=255, blank=True, null=True)
    encoding = models.CharField(max_length=50, blank=True, null=True)
    uploaded = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'controller_fileupload2'


class ControllerHeatcirc(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    item_id = models.IntegerField(primary_key=True)
    settings = models.TextField(blank=True, null=True)
    time = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'controller_heatcirc'
        unique_together = (('controller_id', 'item_id'),)


class ControllerHeatcircSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    force_update = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_heatcirc_sync'


class ControllerHeatgen(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    item_id = models.IntegerField(primary_key=True)
    settings = models.TextField(blank=True, null=True)
    time = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'controller_heatgen'
        unique_together = (('controller_id', 'item_id'),)


class ControllerHeatgenSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    force_update = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_heatgen_sync'


class ControllerIni(models.Model):
    controller_id = models.IntegerField()
    name = models.CharField(max_length=20)
    content = models.TextField()
    user_id = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_ini'


class ControllerIniSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    force_update = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_ini_sync'


class ControllerIniType(models.Model):
    name = models.CharField(primary_key=True, max_length=30)

    class Meta:
        managed = False
        db_table = 'controller_ini_type'


class ControllerIologs(models.Model):
    controller_id = models.IntegerField(blank=True, null=True)
    timestamp = models.IntegerField(blank=True, null=True)
    month = models.SmallIntegerField(blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    time = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs'


class ControllerIologsDhw(models.Model):
    module_id = models.CharField(primary_key=True, max_length=23)
    element_id = models.SmallIntegerField()
    on_off = models.IntegerField(blank=True, null=True)
    eco = models.IntegerField(blank=True, null=True)
    party = models.IntegerField(blank=True, null=True)
    freze = models.IntegerField(blank=True, null=True)
    optimization = models.IntegerField(blank=True, null=True)
    schedule = models.IntegerField(blank=True, null=True)
    t_desired = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    t_real = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    output = models.SmallIntegerField(blank=True, null=True)
    reserved = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs_dhw'
        unique_together = (('module_id', 'element_id'),)


class ControllerIologsHc(models.Model):
    module_id = models.CharField(primary_key=True, max_length=23)
    element_id = models.SmallIntegerField()
    hc_mode = models.IntegerField(blank=True, null=True)
    on_off = models.IntegerField(blank=True, null=True)
    eco = models.IntegerField(blank=True, null=True)
    party = models.IntegerField(blank=True, null=True)
    freze = models.IntegerField(blank=True, null=True)
    optimization = models.IntegerField(blank=True, null=True)
    schedule = models.IntegerField(blank=True, null=True)
    t_desired = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    t_real = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    output = models.SmallIntegerField(blank=True, null=True)
    reserved = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs_hc'
        unique_together = (('module_id', 'element_id'),)


class ControllerIologsHeatgen(models.Model):
    module_id = models.CharField(primary_key=True, max_length=23)
    element_id = models.SmallIntegerField()
    on_off = models.IntegerField(blank=True, null=True)
    eco = models.IntegerField(blank=True, null=True)
    party = models.IntegerField(blank=True, null=True)
    freze = models.IntegerField(blank=True, null=True)
    optimization = models.IntegerField(blank=True, null=True)
    schedule = models.IntegerField(blank=True, null=True)
    t_desired = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    t_real = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    output = models.SmallIntegerField(blank=True, null=True)
    reserved = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs_heatgen'
        unique_together = (('module_id', 'element_id'),)


class ControllerIologsLights(models.Model):
    module_id = models.CharField(primary_key=True, max_length=23)
    element_id = models.SmallIntegerField()
    state = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    reserved = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs_lights'
        unique_together = (('module_id', 'element_id'),)


class ControllerIologsLouvres(models.Model):
    module_id = models.CharField(primary_key=True, max_length=23)
    element_id = models.SmallIntegerField()
    state = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    reserved = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs_louvres'
        unique_together = (('module_id', 'element_id'),)


class ControllerIologsModule(models.Model):
    module_id = models.CharField(primary_key=True, max_length=23)
    iolog_id = models.IntegerField(blank=True, null=True)
    timestamp = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs_module'


class ControllerIologsTask(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    task_status = models.IntegerField(blank=True, null=True)
    start_time = models.IntegerField(blank=True, null=True)
    end_time = models.IntegerField(blank=True, null=True)
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'controller_iologs_task'


class ControllerIologsTsensors(models.Model):
    module_id = models.CharField(primary_key=True, max_length=23)
    t_sensor_id = models.SmallIntegerField()
    temp = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_iologs_tsensors'
        unique_together = (('module_id', 't_sensor_id'),)


class ControllerItemType(models.Model):
    item_type = models.CharField(primary_key=True, max_length=50)
    item_name = models.CharField(unique=True, max_length=50)
    timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'controller_item_type'


class ControllerLogError(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    error_data = models.TextField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_log_error'


class ControllerLogsHeatmeters(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    usr_id = models.IntegerField(primary_key=True)
    energy = models.FloatField()
    power = models.FloatField()
    volume = models.FloatField()
    volume_flow = models.FloatField()
    flow_temp = models.FloatField()
    return_temp = models.FloatField()
    temp_diff = models.FloatField()

    class Meta:
        managed = True
        db_table = 'controller_logs_heatmeters'
        unique_together = (('controller_id', 'time', 'usr_id'),)


class ControllerLogsAi(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.SmallIntegerField(primary_key=True)
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'controller_logs_ai'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsAo(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.SmallIntegerField(primary_key=True)
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'controller_logs_ao'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsDhw(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField()
    element_id = models.SmallIntegerField()
    on_off = models.IntegerField(blank=True, null=True)
    eco = models.IntegerField(blank=True, null=True)
    party = models.IntegerField(blank=True, null=True)
    freeze = models.IntegerField(blank=True, null=True)
    optimization = models.IntegerField(blank=True, null=True)
    schedule = models.IntegerField(blank=True, null=True)
    t_desired = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    t_real = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    output = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_dhw'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsDi(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.SmallIntegerField(primary_key=True)
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'controller_logs_di'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsDo(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.SmallIntegerField(primary_key=True)
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'controller_logs_do'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsHc(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField()
    element_id = models.SmallIntegerField()
    hc_mode = models.IntegerField(blank=True, null=True)
    on_off = models.IntegerField(blank=True, null=True)
    eco = models.IntegerField(blank=True, null=True)
    party = models.IntegerField(blank=True, null=True)
    freeze = models.IntegerField(blank=True, null=True)
    optimization = models.IntegerField(blank=True, null=True)
    schedule = models.IntegerField(blank=True, null=True)
    t_desired = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    t_real = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    output = models.SmallIntegerField(blank=True, null=True)
    t_flow = models.FloatField(blank=True, null=True)
    t_flow_des = models.FloatField()

    class Meta:
        managed = False
        db_table = 'controller_logs_hc'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsHeatcircuit(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.SmallIntegerField(primary_key=True)
    status = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_heatcircuit'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsHeatgen(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.SmallIntegerField(primary_key=True)
    t_desired = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    t_real = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    output = models.SmallIntegerField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_heatgen'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsLights(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.IntegerField(primary_key=True)
    light_state = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_lights'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsLouvres(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.IntegerField(primary_key=True)
    louvre_state = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_louvres'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerLogsOutdoorLocal(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    value = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_outdoor_local'
        unique_together = (('controller_id', 'time'),)


class ControllerLogsSystem(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    system_mode_s_on_off = models.IntegerField(blank=True, null=True)
    system_mode_s_vacation = models.IntegerField(blank=True, null=True)
    system_mode_s_away = models.IntegerField(blank=True, null=True)
    system_mode_s_home = models.IntegerField(blank=True, null=True)
    system_mode_h_hc_mode = models.IntegerField(blank=True, null=True)
    system_mode_h_on_off = models.IntegerField(blank=True, null=True)
    system_mode_h_eco = models.IntegerField(blank=True, null=True)
    system_mode_h_party = models.IntegerField(blank=True, null=True)
    system_mode_h_optimization = models.IntegerField(blank=True, null=True)
    system_mode_w_on_off = models.IntegerField(blank=True, null=True)
    system_mode_w_party = models.IntegerField(blank=True, null=True)
    system_mode_w_eco = models.IntegerField(blank=True, null=True)
    system_mode_w_optimization = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_system'
        unique_together = (('controller_id', 'time'),)


class ControllerLogsTsensors(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField(primary_key=True)
    element_id = models.SmallIntegerField(primary_key=True)
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_logs_tsensors'
        unique_together = (('controller_id', 'time', 'element_id'),)


class ControllerProfiles(models.Model):
    controller_id = models.IntegerField()
    item_id = models.IntegerField()
    profile_name = models.CharField(max_length=30)
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_profiles'


class ControllerProfilesSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    force_update = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_profiles_sync'


class ControllerSchedule(models.Model):
    controller_id = models.IntegerField()
    profile_id = models.IntegerField()
    item_id = models.IntegerField()
    type = models.ForeignKey(ControllerItemType, models.DO_NOTHING, db_column='type')
    mode = models.CharField(max_length=20, blank=True, null=True)
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_schedule'


class ControllerScheduleInterval(models.Model):
    schedule_id = models.IntegerField(primary_key=True)
    enabled = models.IntegerField()
    day = models.CharField(max_length=3, primary_key=True)
    hour = models.IntegerField(primary_key=True)
    min = models.IntegerField(primary_key=True)
    set_temp = models.DecimalField(max_digits=5, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'controller_schedule_interval'
        unique_together = (('schedule_id', 'day', 'hour', 'min'),)


class ControllerScheduleSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    force_update = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_schedule_sync'


class ControllerStatus(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    time = models.IntegerField()
    log_time = models.IntegerField(default=0)
    d_output = models.TextField(blank=True, null=True)
    d_input = models.TextField(blank=True, null=True)
    a_output = models.TextField(blank=True, null=True)
    a_input = models.TextField(blank=True, null=True)
    t_sensors = models.TextField(blank=True, null=True)
    hc_zones = models.TextField(blank=True, null=True)
    dhw = models.TextField(blank=True, null=True)
    heat_generator = models.TextField(blank=True, null=True)
    heat_circuits = models.TextField(blank=True, null=True)
    outdoor_local = models.TextField(blank=True, null=True)
    outdoor_remote = models.TextField(blank=True, null=True)
    lights = models.TextField(blank=True, null=True)
    louvres = models.TextField(blank=True, null=True)
    system = models.TextField(blank=True, null=True)
    xml_string = models.TextField(blank=True, null=True)
    modbus_devices = models.TextField(blank=True, null=True)
    errors = models.TextField(blank=True, null=True)
    meters = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'controller_status'


class ControllerSystem(models.Model):
    controller_id = models.IntegerField(blank=True, null=True)
    system_settings = models.TextField(blank=True, null=True)
    time = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'controller_system'


class ControllerSystemSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    force_update = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'controller_system_sync'


class ControllerUserSettings(models.Model):
    type = models.CharField(max_length=15)
    controller_id = models.IntegerField()
    item_id = models.SmallIntegerField()
    name = models.CharField(max_length=30)
    hc_mode = models.CharField(max_length=20, blank=True, null=True)
    mode = models.CharField(max_length=20, blank=True, null=True)
    enabled = models.IntegerField(default=0)
    freeze_prot = models.IntegerField(default=0)
    party_tset = models.DecimalField(max_digits=5, decimal_places=2)
    eco_tset = models.DecimalField(max_digits=5, decimal_places=2)
    freeze_tset = models.DecimalField(max_digits=5, decimal_places=2)
    party_cool_tset = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    eco_cool_tset = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    outcmp = models.IntegerField(blank=True, null=True, default=0)
    outcmp_coeff = models.FloatField(blank=True, null=True, default=0)
    outcmp_limit = models.FloatField(blank=True, null=True, default=0)
    outcmp_hrs = models.FloatField(blank=True, null=True, default=0)
    time = models.IntegerField()
    optimization = models.IntegerField(default=0)

    class Meta:
        managed = True
        db_table = 'controller_user_settings'
        unique_together = (('type', 'controller_id', 'item_id', 'hc_mode'),)


class ControllerUserSettingsSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    force_update = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'controller_user_settings_sync'


class ControllerCommand(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    commands = models.CharField(max_length=255, blank=True, null=True)
    time = models.IntegerField()

    class Meta:
        db_table = 'controller_commands'


class ControllerCommandSync(models.Model):
    controller_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    time = models.IntegerField()

    class Meta:
        db_table = 'controller_commands_sync'


class Countries(models.Model):
    name = models.CharField(max_length=100)
    iso_3166_1_2 = models.CharField(db_column='iso_3166-1-2', max_length=2)  # Field renamed to remove unsuitable characters.
    iso_3166_1_3 = models.CharField(db_column='iso_3166-1-3', max_length=3)  # Field renamed to remove unsuitable characters.

    class Meta:
        managed = False
        db_table = 'countries'


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class TesttblWeather(models.Model):
    controller_id = models.IntegerField()
    hour = models.IntegerField(blank=True, null=True)
    outdoor_temperature = models.FloatField()

    class Meta:
        managed = False
        db_table = 'testtbl_weather'


class WeatherCurrentData(models.Model):
    city_id = models.IntegerField()
    data_time = models.IntegerField(blank=True, null=True)
    temp = models.FloatField(blank=True, null=True)
    temp_avg = models.FloatField(blank=True, null=True)
    temp_max = models.FloatField(blank=True, null=True)
    temp_min = models.FloatField(blank=True, null=True)
    relative_humidity = models.FloatField(blank=True, null=True)
    relative_humidity_avg = models.FloatField(blank=True, null=True)
    wind_direction = models.SmallIntegerField(blank=True, null=True)
    wind_speed = models.FloatField(blank=True, null=True)
    air_pressure_sea_level = models.FloatField(blank=True, null=True)
    air_pressure_location = models.FloatField(blank=True, null=True)
    air_pressure_location_avg = models.FloatField(blank=True, null=True)
    rain_sum_interval = models.SmallIntegerField(blank=True, null=True)
    rain_sum_utc_12 = models.SmallIntegerField(blank=True, null=True)
    global_sun_radiation = models.SmallIntegerField(blank=True, null=True)
    global_sun_radiation_avg = models.SmallIntegerField(blank=True, null=True)
    temp_grnd = models.FloatField(blank=True, null=True)
    temp_grnd_avg = models.FloatField(blank=True, null=True)
    diff_sun_radiation = models.IntegerField(blank=True, null=True)
    diff_sun_radiation_avg = models.IntegerField(blank=True, null=True)
    clouds_1 = models.FloatField(blank=True, null=True)
    clouds_2 = models.FloatField(blank=True, null=True)
    clouds_3 = models.FloatField(blank=True, null=True)
    clouds_4 = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'weather_current_data'


class ControllerResque(models.Model):
    controller_id = models.AutoField(primary_key=True)
    private_key = models.TextField()
    public_key = models.TextField()
    time = models.IntegerField()

    class Meta:
        db_table= 'controller_resque'
