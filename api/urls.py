from django.conf.urls import url

from api.views import ControllerSettingsView, ControllerStatusView, ControllerResqueView

urlpatterns = [
    url(r'^set', ControllerSettingsView.as_view(), name='controller-settings'),
    url(r'^status', ControllerStatusView.as_view(), name='controller-status'),
    url(r'^resque', ControllerResqueView.as_view(), name='controller-resque'),
]
