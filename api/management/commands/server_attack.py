import os

import grequests
import time
from django.core.management.base import BaseCommand

from api.models import Controller, ControllerSystem, ControllerSystemSync
import xml.etree.ElementTree as ET

from ihpm5.settings import BASE_DIR


class Command(BaseCommand):
    help = "Simulating controllers 'attack' on server"

    def add_arguments(self, parser):
        parser.add_argument('first_controller', type=int)
        parser.add_argument('last_controller', type=int)
        parser.add_argument('timestamp_plus_minus', type=int)
        parser.add_argument('status_settings', type=str, default="both")

    def handle(self, *args, **options):

        settings_url = "https://ihpmdev.i13tech.com/rest/bin/set"
        status_url = "https://ihpmdev.i13tech.com/rest/bin/status/"
        settings_reqs = []
        status_reqs = []

        first_controller = options['first_controller']
        last_controller = options['last_controller']
        timestamp_plus_minus = options['timestamp_plus_minus']
        status_settings = options['status_settings']

        for i in range(first_controller, last_controller + 1):

            timestamp = int(time.time())

            # if Controller.objects.filter(serial_no=i).exists() is False:
            #     print("CONTROLLER {} DOES NOT EXIST".format(i))
            #     object = Controller(serial_no=i, status=1, country_id=204, city_id=37, openweather_city_id=3202781,
            #                         active_profile_id=0, last_report=0, last_change=timestamp, date_created=timestamp,
            #                         auth_wildcard=1)
            #     object.save()
            #
            #     controller_id = Controller.objects.get(serial_no=i).pk
            #     ControllerSystem(controller_id=controller_id, system_settings='{"something":"good"}', time=timestamp).save()
            #     ControllerSystemSync(controller_id=controller_id, user_id=425, force_update=0, time=timestamp).save()
            # else:
            #     print("CONTROLLER {} DOES EXIST".format(i))
            #     controller = Controller.objects.get(serial_no=i)
            #     controller.last_change = timestamp
            #     controller.save()
            #     system_sync = ControllerSystemSync.objects.get(controller_id=controller.pk)
            #     system_sync.time = timestamp
            #     system_sync.save()
            #     system = ControllerSystem.objects.get(controller_id=controller.pk)
            #     system.time = timestamp
            #     system.save()

            data_ver = timestamp + timestamp_plus_minus
            settings_body = """<?xml version="1.0" encoding="UTF-8"?><xml><request><data_ver>{}</data_ver><controller>{}</controller></request></xml>""".format(
                data_ver, i)
            # timestamp + 1 == long polling, timestamp - 1 == get instant response

            settings_reqs.append(grequests.post(settings_url, data=settings_body, verify=False))

            xml_path = os.path.join(BASE_DIR, 'api', 'management', 'commands', 'status_body.xml')

            tree = ET.parse(xml_path)
            root = tree.getroot()
            sn = root.find('.//sn')
            sn.text = str(i)
            status_body = ET.tostring(root).decode('utf-8')

            status_reqs.append(grequests.post(status_url, data=status_body, verify=False))

        if status_settings == "status":
            while True:
                grequests.map(status_reqs)
                time.sleep(15)
        elif status_settings == "settings":
            grequests.map(settings_reqs)
        elif status_settings == "both":
            grequests.map(status_reqs)
            grequests.map(settings_reqs)
