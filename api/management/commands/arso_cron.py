from urllib.request import urlopen

from django.core.management.base import BaseCommand
from pytz import timezone
import datetime
import time
import lxml.etree

from api.models import CitiesWeatherSubscription, WeatherCurrentData, AppCronJobs


class Command(BaseCommand):
    help = "Updating weather data from arso."

    def handle(self, *args, **options):
        url = "http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observationAms_si_latest.xml"

        arsos = CitiesWeatherSubscription.objects.filter(weather_provider="arso_xml")
        urlsocket = urlopen(url)
        root = lxml.etree.parse(urlsocket)
        message = ""

        for arso in arsos:
            provider_id = str(arso.provider_id).strip()
            city_id = int(arso.city_id)
            last_checked_object = arso.last_weather_check

            try:
                path = './/domain_meteosiId[text()="{}"]'.format(provider_id + "_")
                element = root.xpath(path)[0].getparent()
            except:
                try:
                    path = './/domain_shortTitle[text()="{}"]'.format(provider_id)
                    element = root.xpath(path)[0].getparent()
                except:
                    message += "{}, ".format(provider_id)
                    self.stdout.write("ERROR: {} not in arso xml!\n".format(provider_id), ending='')
                    continue

            valid_UTC = str(element.find('valid_UTC').text)
            current_arso_object = datetime.datetime.strptime(valid_UTC, '%d.%m.%Y %H:%M %Z')
            current_arso_object = current_arso_object.replace(tzinfo=timezone('UTC'))

            if current_arso_object > last_checked_object:
                self.stdout.write("SUCCESS: updating {}!\n".format(provider_id), ending='')

                arso.last_weather_check = current_arso_object
                arso.save()

                data_time = int(current_arso_object.timestamp())

                try:
                    temp = float(element.find('t').text)
                except (AttributeError, TypeError):
                    temp = None
                try:
                    temp_avg = float(element.find('tavg').text)
                except (AttributeError, TypeError):
                    temp_avg = None
                try:
                    temp_max = float(element.find('tx').text)
                except (AttributeError, TypeError):
                    temp_max = None
                try:
                    temp_min = float(element.find('tn').text)
                except (AttributeError, TypeError):
                    temp_min = None
                try:
                    relative_humidity = int(element.find('rh').text)
                except (AttributeError, TypeError):
                    relative_humidity = None
                try:
                    relative_humidity_avg = int(element.find('rhavg').text)
                except (AttributeError, TypeError):
                    relative_humidity_avg = None
                try:
                    wind_direction = int(element.find('ddavg_val').text)
                except (AttributeError, TypeError):
                    wind_direction = None
                try:
                    wind_speed = float(element.find('ffavg_val').text)
                except (AttributeError, TypeError):
                    wind_speed = None
                try:
                    air_pressure_sea_level = float(element.find('msl').text)
                except (AttributeError, TypeError):
                    air_pressure_sea_level = None
                try:
                    air_pressure_location = float(element.find('p').text)
                except (AttributeError, TypeError):
                    air_pressure_location = None
                try:
                    air_pressure_location_avg = float(element.find('pavg').text)
                except (AttributeError, TypeError):
                    air_pressure_location_avg = None
                try:
                    rain_sum_interval = float(element.find('rr_val').text)
                except (AttributeError, TypeError):
                    rain_sum_interval = None
                try:
                    rain_sum_utc_12 = float(element.find('tp_12h_acc').text)
                except (AttributeError, TypeError):
                    rain_sum_utc_12 = None
                try:
                    global_sun_radiation = int(element.find('gSunRad').text)
                except (AttributeError, TypeError):
                    global_sun_radiation = None
                try:
                    global_sun_radiation_avg = int(element.find('gSunRadavg').text)
                except (AttributeError, TypeError):
                    global_sun_radiation_avg = 0
                try:
                    temp_grnd = float(element.find('tg_5_cm').text)
                except (AttributeError, TypeError):
                    temp_grnd = 0
                try:
                    temp_grnd_avg = float(element.find('tgavg_5_cm').text)
                except (AttributeError, TypeError):
                    temp_grnd_avg = 0
                try:
                    diff_sun_radiation = int(element.find('diffSunRad').text)
                except (AttributeError, TypeError):
                    diff_sun_radiation = 0
                try:
                    diff_sun_radiation_avg = int(element.find('diffSunRadavg').text)
                except (AttributeError, TypeError):
                    diff_sun_radiation_avg = 0
                try:
                    clouds_1 = float(element.find('ns1_val').text)
                except (AttributeError, TypeError):
                    clouds_1 = 0
                try:
                    clouds_2 = float(element.find('ns2_val').text)
                except (AttributeError, TypeError):
                    clouds_2 = 0
                try:
                    clouds_3 = float(element.find('ns3_val').text)
                except (AttributeError, TypeError):
                    clouds_3 = 0
                try:
                    clouds_4 = float(element.find('ns4_val').text)
                except (AttributeError, TypeError):
                    clouds_4 = 0

                WeatherCurrentData(city_id=city_id, data_time=data_time, temp=temp, temp_avg=temp_avg, temp_max=temp_max,
                                   temp_min=temp_min,relative_humidity=relative_humidity,
                                   relative_humidity_avg=relative_humidity_avg,
                                   wind_direction=wind_direction, wind_speed=wind_speed,
                                   air_pressure_sea_level=air_pressure_sea_level, air_pressure_location=air_pressure_location,
                                   air_pressure_location_avg=air_pressure_location_avg, rain_sum_interval=rain_sum_interval,
                                   rain_sum_utc_12=rain_sum_utc_12, global_sun_radiation=global_sun_radiation,
                                   global_sun_radiation_avg=global_sun_radiation_avg, temp_grnd=temp_grnd,
                                   temp_grnd_avg=temp_grnd_avg, diff_sun_radiation=diff_sun_radiation,
                                   diff_sun_radiation_avg=diff_sun_radiation_avg, clouds_1=clouds_1, clouds_2=clouds_2,
                                   clouds_3=clouds_3, clouds_4=clouds_4).save()
            else:
                self.stdout.write("SUCCESS: {} allready up to date\n".format(provider_id), ending='')

        if not message:
            message = "Current weather successfully updated!"

        job = AppCronJobs.objects.get(job_name="weather_api_arso")
        job.last_run_result = "Errors on followings cities (provider_id): " + message
        job.last_run_time = int(time.time())
        job.job_start_time = time.strftime("%H:%M")
        job.save()



