import json
import os
import subprocess
import xml.etree.ElementTree as ET

import time
from uuid import uuid4

from django.db.models import Max
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.http import HttpResponseNotModified
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from ipware.ip import get_ip, get_real_ip

from api.models import Controller, ControllerSystemSync, ControllerSystem, ControllerHeatcircSync, ControllerHeatcirc, \
    ControllerHeatgen, ControllerHeatgenSync, ControllerProfilesSync, ControllerProfiles, ControllerUserSettingsSync, \
    ControllerUserSettings, ControllerScheduleSync, ControllerSchedule, ControllerStatus, ControllerLogsAi, \
    ControllerLogsDi, ControllerLogsDo, ControllerLogsAo, ControllerLogsTsensors, ControllerLogsLights, \
    ControllerLogsLouvres, WeatherCurrentData, AppUserData, ControllerCommandSync, ControllerCommand, AppLogRest, \
    AppLogError, ControllerFileupload2, ControllerResque
from api.utils.db2xml import system_sett2xml, get_controller_version, profiles2xml, user_setts2xml, schedules2xml, \
    weather2xml, gencirc2xml, commands2xml, files2xml
from api.utils.xml2db import sensors2json, append2logs, merge2modbus, settings2db, circ2db, gen2db, profiles2db, \
    user_setts2db, \
    dhw2logs, schedules2db, hc2logs, system2logs, heatgen2logs, heatcirc2logs, outdoor_local2logs, files2db, \
    meters2json, \
    meters2db


@method_decorator(csrf_exempt, name='dispatch')
class ControllerSettingsView(View):
    model = Controller

    def __init__(self):
        self.long_polling = 120
        self.path_info = None
        self.ip = None
        self.request_method = None
        self.controller = None

    def log_error(self, message):
        timestamp = int(time.time())
        AppLogError(time=timestamp, ip=self.ip, uri=self.path_info, message=message).save()

    def generate_token(self):
        generated_access_token = uuid4()
        generated_expires_at = int(time.time()) + 86400
        self.controller.auth_access_token = str(generated_access_token)
        self.controller.auth_expires_at = generated_expires_at
        self.controller.auth_wildcard = 0
        self.controller.save()
        access_token_element = ET.Element("auth_access_token")
        access_token_element.text = str(generated_access_token)
        return ET.tostring(access_token_element).decode('utf-8')

    def post(self, request, *args, **kwargs):
        if request.is_secure and request.scheme == "https":

            timestamp = int(time.time())

            self.ip = get_real_ip(request)
            if self.ip is None:
                self.ip = get_ip(request)

            self.path_info = request.META["PATH_INFO"]
            self.request_method = request.META["REQUEST_METHOD"]

            parser = ET.XMLParser(encoding="utf-8")
            xml_data = ET.fromstring(request.body, parser=parser)

            try:
                controller_token = str(xml_data.find('.//auth_access_token').text)
            except (AttributeError, TypeError):
                controller_token = 0

            try:
                controller_serial = int(xml_data.find('.//controller').text)
            except (AttributeError, TypeError):
                self.log_error("Controller didn't provide it's serial number.")
                return HttpResponse(
                    "<?xml version='1.0' encoding='UTF-8'?><xml><info_message>You must provide a serial number!!</info_message></xml>")

            try:
                self.controller = Controller.objects.get(serial_no=controller_serial)
            except Controller.DoesNotExist:
                self.log_error("Controller with serial number {} is not registered in the 'controller' table.".format(
                    controller_serial))
                return HttpResponse(
                    "<?xml version='1.0' encoding='UTF-8'?><xml><info_message>Controller with serial number {} does not exist or is not yet registered!</info_message></xml>".format(
                        controller_serial))

            controller_id = int(self.controller.pk)

            self.controller.last_report = timestamp
            self.controller.save()

            AppLogRest(controller_sn=controller_serial, uri=self.path_info, method=self.request_method,
                       ip_address=self.ip, time=timestamp).save()

            auth_token = str(self.controller.auth_access_token)
            auth_wildcard = int(self.controller.auth_wildcard)
            auth_expires_at = int(self.controller.auth_expires_at)

            if auth_wildcard == 0 or auth_wildcard == "None":
                if controller_token != auth_token:
                    return HttpResponseForbidden()
                elif timestamp > auth_expires_at:
                    return_auth = self.generate_token()
                    return HttpResponse("<?xml version='1.0' encoding='UTF-8'?><xml>{}</xml>".format(return_auth))
            elif auth_wildcard == 2:
                return_auth = self.generate_token()
                return HttpResponse("<?xml version='1.0' encoding='UTF-8'?><xml>{}</xml>".format(return_auth))

            try:
                version = int(xml_data.find('.//data_ver').text)
            except (AttributeError, TypeError):
                version = 1
            try:
                weather_version = int(xml_data[0].find('.//weather_time').text)
            except (AttributeError, TypeError):
                weather_version = int(time.time()) + 1

            controller_city_id = self.controller.city_id

            for i in range(self.long_polling):
                if int(self.controller.status) == 1:

                    timestamp = int(time.time())
                    if auth_wildcard == 0 and timestamp > auth_expires_at:
                        return_auth = self.generate_token()
                        return HttpResponse("<?xml version='1.0' encoding='UTF-8'?><xml>{}</xml>".format(return_auth))

                    system_sync_ver = get_controller_version(ControllerSystemSync, controller_id)
                    heatcirc_sync_ver = get_controller_version(ControllerHeatcircSync, controller_id)
                    heatgen_sync_ver = get_controller_version(ControllerHeatgenSync, controller_id)
                    profiles_sync_ver = get_controller_version(ControllerProfilesSync, controller_id)
                    user_settings_sync_ver = get_controller_version(ControllerUserSettingsSync, controller_id)
                    schedule_sync_ver = get_controller_version(ControllerScheduleSync, controller_id)
                    commands_sync_ver = get_controller_version(ControllerCommandSync, controller_id)
                    weather_sync_ver = WeatherCurrentData.objects.filter(city_id=controller_city_id) \
                        .aggregate(Max('data_time'))['data_time__max']

                    if weather_sync_ver is None:
                        weather_sync_ver = -1
                        message = "There is no data for city_id {} in table <weather_current_data>".format(controller_city_id)
                        if AppLogError.objects.filter(ip=self.ip, uri=self.path_info, message=message).exists() is False:
                            self.log_error(message)

                    versions_models = [ControllerSystemSync, ControllerHeatcircSync, ControllerHeatgenSync,
                                       ControllerProfilesSync, ControllerUserSettingsSync, ControllerScheduleSync,
                                       ControllerCommandSync]
                    versions = [system_sync_ver, heatcirc_sync_ver, heatgen_sync_ver, profiles_sync_ver,
                                user_settings_sync_ver, schedule_sync_ver, commands_sync_ver]
                    max_ver = max(versions)

                    max_index = versions.index(max(versions))
                    try:
                        user_id = versions_models[max_index].objects.get(controller_id=controller_id).user_id
                    except versions_models[max_index].DoesNotExist:
                        user_id = 0

                    full_xml = ""
                    w = 0

                    files_to_send = ControllerFileupload2.objects.filter(controller_id=controller_id, uploaded=0)

                    if system_sync_ver > version:
                        try:
                            system_sett = ControllerSystem.objects.get(controller_id=controller_id).system_settings
                        except ControllerSystem.DoesNotExist:
                            system_sett = "[]"
                        system_sett_xml = system_sett2xml("system_sett", system_sett)
                        full_xml += system_sett_xml

                    if heatcirc_sync_ver > version:
                        heatcircs = ControllerHeatcirc.objects.filter(controller_id=controller_id, time__gt=version)
                        heatcirc_sett_xml = gencirc2xml("circ_user_sett", heatcircs)
                        full_xml += heatcirc_sett_xml.decode('utf-8')

                    if heatgen_sync_ver > version:
                        heatgens = ControllerHeatgen.objects.filter(controller_id=controller_id, time__gt=version)
                        heatgen_sett_xml = gencirc2xml("hgen_user_sett", heatgens)
                        full_xml += heatgen_sett_xml.decode('utf-8')

                    if profiles_sync_ver > version:
                        profiles = ControllerProfiles.objects.filter(controller_id=controller_id, time__gt=version)
                        active_profile = self.controller.active_profile_id
                        profiles_sett_xml = profiles2xml(active_profile, profiles)
                        full_xml += profiles_sett_xml.decode('utf-8')

                    if user_settings_sync_ver > version:
                        settings = ControllerUserSettings.objects.filter(controller_id=controller_id, time__gt=version)
                        user_setts_xml = user_setts2xml(settings)
                        full_xml += user_setts_xml

                    if schedule_sync_ver > version:
                        schedules = ControllerSchedule.objects.filter(controller_id=controller_id, time__gt=version)
                        schedules_xml = schedules2xml(schedules)
                        full_xml += schedules_xml

                    if commands_sync_ver > version:
                        try:
                            commands = ControllerCommand.objects.get(controller_id=controller_id).commands
                        except ControllerCommand.DoesNotExist:
                            commands = "[]"
                        commands_xml = commands2xml(commands)
                        full_xml += commands_xml

                    if files_to_send:
                        files_xml = files2xml(files_to_send, controller_id, timestamp, self.ip, self.path_info)
                        full_xml += files_xml

                    if weather_sync_ver > weather_version:
                        weather = WeatherCurrentData.objects.get(city_id=controller_city_id, data_time=weather_sync_ver)
                        weather_xml = weather2xml(weather)
                        if len(full_xml) == 0:
                            w = 1
                        full_xml += weather_xml.decode('utf-8')

                    if xml_data.find('.//system_sett'):
                        try:
                            settings2db(xml_data, controller_id, timestamp)
                        except:
                            self.log_error(
                                "An error occurred while trying to save <system_sett> into 'system_settings' table for controller with id: {}.".format(
                                    controller_id))

                    if xml_data.find('.//heatcirc_sett') or xml_data.find('.//heatcirc_setts'):
                        try:
                            circ2db(xml_data, controller_id, timestamp)
                        except:
                            self.log_error(
                                "An error occurred while trying to save <heatcirc_setts> into 'controller_heatcirc' table for controller with id: {}.".format(
                                    controller_id))

                    if xml_data.find('.//heatgen_sett') or xml_data.find('.//heatgen_setts'):
                        try:
                            gen2db(xml_data, controller_id, timestamp)
                        except:
                            self.log_error(
                                "An error occurred while trying to save <heatgen_setts> into 'controller_heatgen' table for controller with id: {}.".format(
                                    controller_id))

                    if xml_data.find('.//profiles_sett') or xml_data.find('.//profiles'):
                        try:
                            profiles2db(xml_data, controller_id, timestamp)
                        except:
                            self.log_error(
                                "An error occurred while trying to save <profiles_setts> into 'controller_profiles' table for controller with id: {}.".format(
                                    controller_id))

                    if xml_data.find('.//hc_user_setts') or xml_data.find('.//hc_user_sett'):
                        try:
                            user_setts2db(xml_data, controller_id, timestamp, "uSett_hc")
                        except:
                            self.log_error(
                                "An error occurred while trying to save <hc_user_setts> into 'controller_users_settings' table for controller with id: {}.".format(
                                    controller_id))

                    if xml_data.find('.//dhw_user_setts') or xml_data.find('.//dhw_user_sett'):
                        try:
                            user_setts2db(xml_data, controller_id, timestamp, "uSett_dhw")
                        except:
                            self.log_error(
                                "An error occurred while trying to save <dhw_user_setts> into 'controller_users_settings' table for controller with id: {}.".format(
                                    controller_id))

                    if xml_data.find('.//hc_user_schedules') or xml_data.find('.//hc_user_schedule'):
                        try:
                            schedules2db(xml_data, controller_id, timestamp, "sch_hc")
                        except:
                            self.log_error(
                                "An error occurred while trying to save <hc_user_schedules> into 'controller_schedule' table for controller with id: {}.".format(
                                    controller_id))

                    if xml_data.find('.//files') or xml_data.find('.//file'):
                        try:
                            files2db(xml_data, controller_id, timestamp)
                        except:
                            self.log_error(
                                "An error occurred while trying to save <files> into 'controller_ini' table for controller with id: {}.".format(
                                    controller_id))

                    if len(full_xml) > 0:
                        if w == 0:
                            try:
                                username = AppUserData.objects.get(user_id=user_id).username
                            except AppUserData.DoesNotExist:
                                username = "unknown"
                            full_xml = "<?xml version='1.0' encoding='UTF-8'?><xml><data_ver>{}</data_ver><username>{}</username>".format(
                                max_ver, username) + full_xml + "</xml>"
                        elif w == 1:
                            full_xml = "<?xml version='1.0' encoding='UTF-8'?><xml>" + full_xml + "</xml>"
                        return HttpResponse(full_xml)
                    elif i % 10 == 0:
                        print("{} controller has been polling for {} seconds.".format(controller_id, i*5))
                else:
                    return HttpResponse(
                        "<?xml version='1.0' encoding='UTF-8'?><xml><info_message>Controller with id {} is offline!</info_message></xml>".format(
                            controller_id))
                time.sleep(5)
            return HttpResponseNotModified()
        else:
            return HttpResponseForbidden()


@method_decorator(csrf_exempt, name='dispatch')
class ControllerStatusView(View):
    model = ControllerStatus

    def __init__(self):
        self.path_info = None
        self.ip = None
        self.request_method = None

    def log_error(self, message):
        timestamp = int(time.time())
        AppLogError(time=timestamp, ip=self.ip, uri=self.path_info, message=message).save()

    def post(self, request, *args, **kwargs):
        # if request.is_secure and request.scheme == "https":
        if request.is_secure:

            version = int(time.time())

            self.ip = get_real_ip(request)
            if self.ip is None:
                self.ip = get_ip(request)

            self.path_info = request.META["PATH_INFO"]
            self.request_method = request.META["REQUEST_METHOD"]

            parser = ET.XMLParser(encoding="utf-8")
            xml_data = ET.fromstring(request.body, parser=parser)

            try:
                controller_token = str(xml_data.find('.//auth_access_token').text)
            except (AttributeError, TypeError):
                controller_token = 0

            try:
                controller_serial = int(xml_data.find('.//controller').text)
            except (AttributeError, TypeError):
                try:
                    controller_serial = int(xml_data.find('.//sn').text)
                except (AttributeError, TypeError):
                    self.log_error("Controller didn't provide it's serial number.")
                    return HttpResponse(status=204)

            try:
                controller = Controller.objects.get(serial_no=controller_serial)
            except Controller.DoesNotExist:
                self.log_error("Controller with serial number {} is not registered in the 'controller' table.".format(
                    controller_serial))
                return HttpResponse(status=204)

            controller_id = int(controller.pk)
            controller.last_report = version
            controller.save()

            AppLogRest(controller_sn=controller_serial, uri=self.path_info, method=self.request_method,
                       ip_address=self.ip, time=version).save()

            auth_token = str(controller.auth_access_token)
            auth_wildcard = int(controller.auth_wildcard)
            auth_expires_at = int(controller.auth_expires_at)
            timestamp = int(time.time())

            if auth_wildcard == 0 or auth_wildcard == "None":
                if controller_token != auth_token:
                    return HttpResponseForbidden()
                elif timestamp > auth_expires_at:
                    return HttpResponseForbidden()

            if ControllerStatus.objects.filter(controller_id=controller_id).exists():
                status = ControllerStatus.objects.get(controller_id=controller_id)
                status_sync_ver = status.log_time
            else:
                status = ControllerStatus(controller_id=controller_id)
                status_sync_ver = 0

            d_output = sensors2json(xml_data, "do")
            d_input = sensors2json(xml_data, "di")
            a_output = sensors2json(xml_data, "ao")
            a_input = sensors2json(xml_data, "ai")
            t_sensors = sensors2json(xml_data, "t_sensor")
            hc_zones = sensors2json(xml_data, "hc")
            dhw = sensors2json(xml_data, "dhw")
            heat_generator = sensors2json(xml_data, "heat_generator")
            heat_circuits = sensors2json(xml_data, "heat_circuit")
            outdoor_local = sensors2json(xml_data, "outdoor_local")
            outdoor_remote = sensors2json(xml_data, "outdoor_remote")
            lights = sensors2json(xml_data, "light")
            louvres = sensors2json(xml_data, "louvre")
            system = sensors2json(xml_data, "system")
            modbus = sensors2json(xml_data, "modbus_device")
            cards = sensors2json(xml_data, "card")
            if cards:
                modbus_devices = merge2modbus(modbus, cards)
            else:
                modbus_devices = modbus
            errors = sensors2json(xml_data, "error")
            meters = meters2json(xml_data)

            status.time = version
            if d_input:
                status.d_input = json.dumps(d_input)
            if d_output:
                status.d_output = json.dumps(d_output)
            if a_output:
                status.a_output = json.dumps(a_output)
            if a_input:
                status.a_input = json.dumps(a_input)
            if t_sensors:
                status.t_sensors = json.dumps(t_sensors)
            if hc_zones:
                status.hc_zones = json.dumps(hc_zones)
            if dhw:
                status.dhw = json.dumps(dhw)
            if heat_generator:
                status.heat_generator = json.dumps(heat_generator)
            if heat_circuits:
                status.heat_circuits = json.dumps(heat_circuits)
            if outdoor_local:
                status.outdoor_local = json.dumps(outdoor_local)
            if outdoor_remote:
                status.outdoor_remote = json.dumps(outdoor_remote[0])
            if lights:
                status.lights = json.dumps(lights)
            if louvres:
                status.louvres = json.dumps(louvres)
            if system:
                status.system = json.dumps(system[0])
            if modbus_devices:
                status.modbus_devices = json.dumps(modbus_devices)
            if errors:
                status.errors = json.dumps(errors)
            if meters:
                status.meters = json.dumps(meters)

                try:
                    meters2db(controller_id, meters)
                except:
                    self.log_error(
                        "error: 'meters to logs', table: 'controller_logs_heatmeters', controller_id: {}, data: {}".format(
                            controller_id, meters))

            try:
                status.save()
            except:
                self.log_error("Couldn't update status in table 'controller_status' for controller with id: {}.".format(
                    controller_id))

            print("controller_id: " + str(controller_id) + ", now: " + str(version) + ", status_sync_ver: " + str(
                status_sync_ver))
            if (status_sync_ver + 60) <= version:
                print("UPDATING LOGS!")
                if d_input:
                    try:
                        append2logs(d_input, ControllerLogsDi, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_di', controller_id: {}, data: {}".format(
                                controller_id, d_input))

                if d_output:
                    try:
                        append2logs(d_output, ControllerLogsDo, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_do', controller_id: {}, data: {}".format(
                                controller_id, d_output))

                if a_input:
                    try:
                        append2logs(a_input, ControllerLogsAi, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_ai', controller_id: {}, data: {}".format(
                                controller_id, a_input))

                if a_output:
                    try:
                        append2logs(a_output, ControllerLogsAo, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_ao', controller_id: {}, data: {}".format(
                                controller_id, a_output))

                if t_sensors:
                    try:
                        append2logs(t_sensors, ControllerLogsTsensors, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_tsensors', controller_id: {}, data: {}".format(
                                controller_id, t_sensors))

                if lights:
                    try:
                        append2logs(lights, ControllerLogsLights, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_lights', controller_id: {}, data: {}".format(
                                controller_id, lights))

                if louvres:
                    try:
                        append2logs(louvres, ControllerLogsLouvres, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_louvres', controller_id: {}, data: {}".format(
                                controller_id, louvres))
                if dhw:
                    try:
                        dhw2logs(dhw, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_dhw', controller_id: {}, data: {}".format(
                                controller_id, dhw))
                if hc_zones:
                    try:
                        hc2logs(hc_zones, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_hc', controller_id: {}, data: {}".format(
                                controller_id, hc_zones))

                if system:
                    try:
                        system2logs(system, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_system', controller_id: {}, data: {}".format(
                                controller_id, system))

                if heat_generator:
                    try:
                        heatgen2logs(heat_generator, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_heatgen', controller_id: {}, data: {}".format(
                                controller_id, heat_generator))

                if heat_circuits:
                    try:
                        heatcirc2logs(heat_circuits, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_heatcircuit', controller_id: {}, data: {}".format(
                                controller_id, heat_circuits))

                if outdoor_local:
                    try:
                        outdoor_local2logs(outdoor_local, controller_id, version)
                    except:
                        self.log_error(
                            "error: 'status to logs', table: 'controller_logs_outdoor_local', controller_id: {}, data: {}".format(
                                controller_id, outdoor_local))

                status = ControllerStatus.objects.get(controller_id=controller_id)
                status.log_time = version
                status.save()

                return HttpResponse(status=204)
            else:
                return HttpResponse(status=204)

        else:
            return HttpResponseForbidden()


@method_decorator(csrf_exempt, name='dispatch')
class ControllerResqueView(View):
    model = Controller

    def __init__(self):
        self.path_info = None
        self.ip = None
        self.request_method = None
        self.controller = None

    def log_error(self, message):
        timestamp = int(time.time())
        AppLogError(time=timestamp, ip=self.ip, uri=self.path_info, message=message).save()

    def post(self, request, *args, **kwargs):
        if request.is_secure:

            timestamp = int(time.time())

            self.ip = get_real_ip(request)
            if self.ip is None:
                self.ip = get_ip(request)

            self.path_info = request.META["PATH_INFO"]
            self.request_method = request.META["REQUEST_METHOD"]

            parser = ET.XMLParser(encoding="utf-8")
            xml_data = ET.fromstring(request.body, parser=parser)

            try:
                controller_serial = int(xml_data.find('.//controller').text)
            except (AttributeError, TypeError):
                self.log_error("Controller didn't provide it's serial number.")
                return HttpResponse(
                    "<?xml version='1.0' encoding='UTF-8'?><xml><info_message>You must provide a serial number!!</info_message></xml>")

            try:
                self.controller = Controller.objects.get(serial_no=controller_serial)
            except Controller.DoesNotExist:
                self.log_error("Controller with serial number {} is not registered in the 'controller' table.".format(
                    controller_serial))
                return HttpResponse(
                    "<?xml version='1.0' encoding='UTF-8'?><xml><info_message>Controller with serial number {} does not exist or is not yet registered!</info_message></xml>".format(
                        controller_serial))

            controller_id = int(self.controller.pk)

            self.controller.last_report = timestamp
            self.controller.save()

            AppLogRest(controller_sn=controller_serial, uri=self.path_info, method=self.request_method,
                       ip_address=self.ip, time=timestamp).save()

            if self.controller.resque == 1:

                private_key = "/home/rescuee/.ssh/controller" + str(controller_serial)
                public_key = private_key + ".pub"

                os.system("sudo ssh-keygen -f {} -t rsa -N ''".format(private_key))
                # try:
                #     subprocess.call(["sudo ssh-keygen -f {} -t rsa -N ''".format(private_key)])
                # except OSError:
                #     print("Cant generate ssh-key because it allready exists.")

                os.system("sudo cat {} >> /home/rescuee/.ssh/authorized_keys".format(public_key))

                with open(private_key, "r") as f:
                    private_content = f.read()

                with open(public_key, "r") as f:
                    public_content = f.read()

                if ControllerResque.objects.filter(controller_id=controller_id).exists():
                    resque = ControllerResque.objects.get(controller_id=controller_id)
                    resque.private_key = private_content
                    resque.public_key = public_content
                    resque.time = timestamp
                    resque.save()
                else:
                    ControllerResque(controller_id=controller_id, private_key=private_content, public_key=public_content,
                                     time=timestamp).save()

                return HttpResponse("<?xml version='1.0' encoding='UTF-8'?><xml><private_key>{}</private_key></xml>".format(private_content))
            else:
                return HttpResponseForbidden()
        else:
            return HttpResponseForbidden()
