import json
from xml.etree.ElementTree import Element, SubElement, tostring

from api.models import ControllerScheduleInterval, AppLogError


def validate_text(string):
    if string is None or string == "None":
        return False
    else:
        return True


def get_controller_version(model, controller_id):
    try:
        version = model.objects.get(controller_id=controller_id).time
        return int(version)
    except model.DoesNotExist:
        version = 0
        return version


def system_sett2xml(root_name, json_obj):
    py_dict = json.loads(json_obj)

    if py_dict:
        root = Element(root_name)
        for key in py_dict:
            if validate_text(py_dict[key]):
                child = SubElement(root, str(key))
                child.text = str(py_dict[key])

        return tostring(root).decode('utf-8')
    else:
        return ""


def gencirc2xml(root_name, objects):
    if objects:
        root = Element(root_name + "s")
        for obj in objects:
            subroot = SubElement(root, root_name)
            sett = json.loads(obj.settings)
            for key in sett:
                if validate_text(sett[key]):
                    child = SubElement(subroot, str(key))
                    child.text = str(sett[key])

        return tostring(root)
    else:
        return ""


def profiles2xml(active_profile, profiles):
    if profiles:
        root = Element("profiles_sett")
        if validate_text(active_profile):
            child = SubElement(root, "active_profile_id")
            child.text = str(active_profile)
        child2 = SubElement(root, "profiles")
        for profile in profiles:
            subchild = SubElement(child2, "profile")
            if validate_text(profile.item_id):
                id = SubElement(subchild, "id")
                id.text = str(profile.item_id)
            if validate_text(profile.profile_name):
                name = SubElement(subchild, "name")
                name.text = str(profile.profile_name)

        return tostring(root)
    else:
        return ""


def user_setts2xml(settings):
    settings_xml = ""
    hc = 0
    dhw = 0

    if settings.filter(type="uSett_hc"):
        hc_root = Element("hc_user_setts")
        hc = 1
    if settings.filter(type="uSett_dhw"):
        dhw_root = Element("dhw_user_setts")
        dhw = 1

    for setting in settings:
        type_name = setting.type
        if type_name == "uSett_hc":
            child = SubElement(hc_root, "hc_user_sett")
        elif type_name == "uSett_dhw":
            child = SubElement(dhw_root, "dhw_user_sett")
        else:
            continue

        data = {"id": setting.item_id, "name": setting.name, "enabled": setting.enabled, "freeze_prot": setting.freeze_prot,
                "hc_mode": setting.hc_mode, "mode": setting.mode, "freeze_tset": setting.freeze_tset,
                "party_tset": setting.party_tset, "eco_tset": setting.eco_tset,
                "party_cool_tset": setting.party_cool_tset, "eco_cool_tset": setting.eco_cool_tset,
                "outcmp": setting.outcmp, "outcmp_hrs": setting.outcmp_hrs, "outcmp_coeff": setting.outcmp_coeff,
                "outcmp_limit": setting.outcmp_limit, "optimization": setting.optimization}

        for key in data:
            if validate_text(data[key]):
                subchild = SubElement(child, key)
                subchild.text = str(data[key])

    if hc == 1:
        settings_xml += tostring(hc_root).decode('utf-8')
    if dhw == 1:
        settings_xml += tostring(dhw_root).decode('utf-8')

    return settings_xml


def get_schedules(root_name, child_name, schedules):

    root = Element(root_name)
    for schedule in schedules:
        schedule_id = schedule.id
        child_hc = SubElement(root, child_name)
        if validate_text(schedule.item_id):
            subchild0 = SubElement(child_hc, "id")
            subchild0.text = str(schedule.item_id)
        if validate_text(schedule.profile_id):
            subchild1 = SubElement(child_hc, "profile_id")
            subchild1.text = str(schedule.profile_id)
        if validate_text(schedule.mode):
            subchild2 = SubElement(child_hc, "type")
            subchild2.text = str(schedule.mode)
        subchild3 = SubElement(child_hc, "intervals")

        intervals = ControllerScheduleInterval.objects.filter(schedule_id=schedule_id)
        for interval in intervals:
            item = SubElement(subchild3, "item")
            if validate_text(interval.day):
                d = SubElement(item, "d")
                d.text = str(interval.day)
            if validate_text(interval.hour):
                h = SubElement(item, "h")
                h.text = str(interval.hour)
            if validate_text(interval.set_temp):
                tset = SubElement(item, "tset")
                tset.text = str(interval.set_temp)
            if validate_text(interval.enabled):
                enabled = SubElement(item, "enabled")
                enabled.text = str(interval.enabled)

    return tostring(root).decode('utf-8')


def schedules2xml(schedules):
    hcs = schedules.filter(type="sch_hc")
    dhws = schedules.filter(type="sch_dhw")
    schedules_xml = ""

    if hcs:
        hcs_xml = get_schedules("hc_user_schedules", "hc_user_schedule", hcs)
        schedules_xml += hcs_xml

    if dhws:
        dhws_xml = get_schedules("dhw_user_schedules", "dhw_user_schedule", dhws)
        schedules_xml += dhws_xml

    return schedules_xml


def weather2xml(weather):

    root = Element("weather")
    model_fields = [f.name for f in weather._meta.get_fields()]

    for field in model_fields:
        if validate_text(str(weather.__dict__[field])):
            child = SubElement(root, field)
            child.text = str(weather.__dict__[field])

    return tostring(root)


def commands2xml(json_obj):
    py_dict = json.loads(json_obj)

    if py_dict:
        root = Element("commands")
        for command in py_dict:
            subroot = SubElement(root, "command")
            for key in command:
                if key == "lights" or key == "louvres" or key == "files" and type(command[key]) is list:
                    subsubroot = SubElement(subroot, key)
                    list_of_dict = command[key]
                    for el in list_of_dict:
                        if type(el) is str:
                            subchild = SubElement(subsubroot, key[:-1])
                            subchild.text = str(el)
                        elif type(el) is dict:
                            subchild = SubElement(subsubroot, key[:-1])
                            for item in el:
                                if validate_text(el[item]):
                                    childchild = SubElement(subchild, str(item))
                                    childchild.text = str(el[item])

                elif validate_text(command[key]):
                    child = SubElement(subroot, str(key))
                    child.text = str(command[key])

        return tostring(root).decode('utf-8')
    else:
        return ""


def files2xml(files, controller_id, timestamp, ip, path_info):

    root = Element("files")
    has_files = 0
    for file in files:
        file_path = str(file.filename_full)
        try:
            with open(file_path, "r") as f:
                content = f.read()
        except:
            AppLogError(time=timestamp, ip=ip, uri=path_info,
                        message="File on path '{}' for controller {} does not exists.".format(file_path,
                                                                                              controller_id)).save()
            continue

        subroot = SubElement(root, "file")
        childname = SubElement(subroot, "name")
        childname.text = str(file.filename)
        childcontent = SubElement(subroot, "content")
        childcontent.text = str(content)

        has_files = 1
        file.uploaded = 1
        file.save()

    if has_files == 1:
        return tostring(root).decode('utf-8')
    else:
        return ""
