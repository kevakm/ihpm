import base64
import json

import time
from django.db import connection

from api.models import ControllerSystem, ControllerHeatcirc, ControllerHeatgen, Controller, ControllerProfiles, \
    ControllerUserSettings, ControllerHeatcircSync, ControllerHeatgenSync, ControllerProfilesSync, \
    ControllerUserSettingsSync, ControllerScheduleSync, ControllerSchedule, ControllerScheduleInterval, \
    ControllerIniSync, ControllerIniType, ControllerIni, AppLogRest, ControllerLogsHeatmeters
from api.utils.db2xml import validate_text


def sql_insert(table_name, custom_field, controller_id, time, element_id, value):
    with connection.cursor() as c:
        c.execute("INSERT INTO " + table_name + " (controller_id, time, element_id, " + custom_field +
                  " ) VALUES (%s,%s,%s,%s)", [controller_id, time, element_id, value])


# For DO, DI, AO, AI, TSENSOR, LOUVRES, LIGHTS
def append2logs(json_data, model, controller_id, time):
    model_fields = [f.name for f in model._meta.get_fields()]
    if "controller_id" in model_fields:
        model_fields.remove("controller_id")
    if "time" in model_fields:
        model_fields.remove("time")

    for dict in json_data:
        if "val" in model_fields or "value" in model_fields:
            value = float(dict["val"])
            custom_field = "value"
        elif "light_state" in model_fields:
            value = float(dict["val"])
            custom_field = "light_state"
        elif "louvre_state" in model_fields:
            value = int(dict["val"])
            custom_field = "louvre_state"

        # print(model._meta.db_table, controller_id, time, int(dict["id"]), value)
        sql_insert(model._meta.db_table, custom_field, controller_id, time, int(dict["id"]), value)


def dhw2logs(json_data, controller_id, time):

    for dhw in json_data:

        if "id" in dhw:
            element_id = int(dhw["id"])
        else:
            continue
        if "enabled" in dhw:
            on_off = int(dhw["enabled"])
        else:
            on_off = 0
        if "mode" in dhw:
            mode = str(dhw["mode"])
            if mode == "party":
                party = 1
                eco = 0
                schedule = 0
            elif mode == "eco":
                eco = 1
                party = 0
                schedule = ""
            elif mode == "normal":
                schedule = 1
                party = 0
                eco = 0
        if "freeze" in dhw:
            freeze = int(dhw["freeze"])
        else:
            freeze = 0
        if "optimization" in dhw:
            optimization = int(dhw["optimization"])
        else:
            optimization = 0
        if "tset" in dhw:
            t_desired = float(dhw["tset"])
        else:
            t_desired = -100.0
        if "treal" in dhw:
            t_real = float(dhw["treal"])
        else:
            t_real = -100.0
        if "output" in dhw:
            output = int(dhw["output"])
        else:
            output = 0

        with connection.cursor() as c:
            c.execute("INSERT INTO controller_logs_dhw (controller_id, time, element_id, on_off, eco, party, freeze,"
                      " optimization, schedule, t_desired, t_real, output ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                      [controller_id, time, element_id, on_off, eco, party, freeze, optimization, schedule, t_desired, t_real, output])


def hc2logs(json_data, controller_id, time):

    for hc in json_data:

        if "id" in hc:
            element_id = int(hc["id"])
        else:
            continue
        if "enabled" in hc:
            on_off = int(hc["enabled"])
        else:
            on_off = 0
        if "mode" in hc:
            mode = str(hc["mode"])
            if mode == "party":
                party = 1
                eco = 0
                schedule = 0
            elif mode == "eco":
                eco = 1
                party = 0
                schedule = ""
            elif mode == "normal":
                schedule = 1
                party = 0
                eco = 0
        if "hc_mode" in hc:
            mode = str(hc["hc_mode"])
            if mode == "heating":
                hc_mode = 1
            else:
                hc_mode = 0
        if "freeze" in hc:
            freeze = int(hc["freeze"])
        else:
            freeze = 0
        if "optimization" in hc:
            optimization = int(hc["optimization"])
        else:
            optimization = 0
        if "tset" in hc:
            if type(hc["tset"]) is float:
                t_desired = float(hc["tset"])
            else:
                t_desired = -100.0
        else:
            t_desired = -100.0
        if "treal" in hc:
            t_real = float(hc["treal"])
        else:
            t_real = -100.0
        if "tflow" in hc:
            t_flow = float(hc["tflow"])
        else:
            t_flow = -100.0
        if "tflow_des" in hc:
            t_flow_des = float(hc["tflow_des"])
        else:
            t_flow_des = -100.0
        if "output" in hc:
            output = int(hc["output"])
        else:
            output = 0

        with connection.cursor() as c:
            c.execute(
                "INSERT INTO controller_logs_hc (controller_id, time, element_id, hc_mode, on_off, eco, party, freeze,"
                " optimization, schedule, t_desired, t_real, output, t_flow, t_flow_des) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                [controller_id, time, element_id, hc_mode, on_off, eco, party, freeze, optimization, schedule, t_desired, t_real, output, t_flow, t_flow_des])


def system2logs(json_data, controller_id, time):

    for system in json_data:
        system_mode_s_on_off = 0
        system_mode_s_vacation = 0
        system_mode_s_away = 0
        system_mode_s_home = 0
        system_mode_h_hc_mode = 0
        system_mode_h_on_off = 0
        system_mode_h_eco = 0
        system_mode_h_party = 0
        system_mode_h_optimization = 0
        system_mode_w_on_off = 0
        system_mode_w_party = 0
        system_mode_w_eco = 0
        system_mode_w_optimization = 0

        if "s_enabled" in system:
            system_mode_s_on_off = int(system["s_enabled"])

        if "s_state" in system:
            state = str(system["s_state"])
            if state == "vacation":
                system_mode_s_vacation = 1
            elif state == "away":
                system_mode_s_away = 1
            elif state == "home":
                system_mode_s_home = 1

        if "h_enabled" in system:
            system_mode_w_on_off = int(system["h_enabled"])

        if "h_hc_mode" in system:
            hc_mode = str(system["h_hc_mode"])
            if hc_mode == "heating":
                system_mode_h_hc_mode = 1

        if "h_mode" in system:
            h_mode = str(system["h_mode"])
            if h_mode == "eco":
                system_mode_h_eco = 1
            elif h_mode == "party":
                system_mode_h_party = 1
            elif h_mode == "opt":
                system_mode_h_optimization = 1

        if "w_enabled" in system:
            system_mode_w_on_off = int(system["w_enabled"])

        if "w_mode" in system:
            w_mode = str(system["w_mode"])
            if w_mode == "eco":
                system_mode_w_eco = 1
            elif w_mode == "party":
                system_mode_w_party = 1
            elif w_mode == "opt":
                system_mode_w_optimization = 1

        with connection.cursor() as c:
            c.execute(
                "INSERT INTO controller_logs_system (controller_id, time, system_mode_s_on_off, system_mode_s_vacation, "
                "system_mode_s_away, system_mode_s_home, system_mode_h_hc_mode, system_mode_h_on_off, system_mode_h_eco, "
                "system_mode_h_party, system_mode_h_optimization, system_mode_w_on_off, system_mode_w_party, "
                "system_mode_w_eco, system_mode_w_optimization) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                [controller_id, time, system_mode_s_on_off, system_mode_s_vacation, system_mode_s_away,
                 system_mode_s_home, system_mode_h_hc_mode, system_mode_h_on_off, system_mode_h_eco,
                 system_mode_h_party, system_mode_h_optimization, system_mode_w_on_off, system_mode_w_party,
                 system_mode_w_eco, system_mode_w_optimization])


def heatgen2logs(json_data, controller_id, time):

    for gen in json_data:
        if "id" in gen:
            element_id = int(gen["id"])
        else:
            continue

        status = {}

        if "output" in gen:
            status["output"] = int(gen["output"])
        if "tdes" in gen:
            status["tdes"] = str(gen["tdes"])
        if "t" in gen:
            status["t"] = str(gen["t"])
        if "info" in gen:
            status["info"] = str(gen["info"])

        status = json.dumps(status)

        with connection.cursor() as c:
            c.execute(
                "INSERT INTO controller_logs_heatgen (controller_id, time, element_id, t_desired, t_real, output, status) VALUES (%s,%s,%s,NULL,NULL,NULL,%s)",
                [controller_id, time, element_id, status])


def heatcirc2logs(json_data, controller_id, time):

    for circ in json_data:
        if "id" in circ:
            element_id = int(circ["id"])
        else:
            continue

        # status = {}
        #
        # if "pump" in circ:
        #     status["pump"] = int(circ["pump"])
        # if "output" in circ:
        #     status["output"] = int(circ["output"])
        # if "tdes" in circ:
        #     status["tdes"] = str(circ["tdes"])
        # if "t" in circ:
        #     status["t"] = str(circ["t"])
        # if "tdesw" in circ:
        #     status["tdesw"] = str(circ["tdesw"])
        # if "info" in circ:
        #     status["info"] = str(circ["info"])
        #
        # status = json.dumps(status)

        status = json.dumps(circ)

        with connection.cursor() as c:
            c.execute(
                "INSERT INTO controller_logs_heatcircuit (controller_id, time, element_id, status) VALUES (%s,%s,%s,%s)",
                [controller_id, time, element_id, status])


def outdoor_local2logs(json_data, controller_id, time):

    for t in json_data:

        if "t" in t:
            t = float(t["t"])

        with connection.cursor() as c:
            c.execute(
                "INSERT INTO controller_logs_outdoor_local (controller_id, time, value) VALUES (%s,%s,%s)",
                [controller_id, time, t])


def sensors2json(xml_data, sensor_name):
    list = []
    for child in xml_data.iter(sensor_name):
        dict = {}
        for element in child:
            if len(element) > 0:
                dict[element.tag] = len(element)
            elif validate_text(element.text):
                try:
                    dict[element.tag] = int(element.text)
                except ValueError:
                    try:
                        dict[element.tag] = float(element.text)
                    except ValueError:
                        dict[element.tag] = str(element.text)
        list.append(dict)
    return list


def merge2modbus(modbus, cards):
    modbus_devices = []
    x = 0
    md_dict = {}
    md_list = []

    for i in range(len(modbus)):
        card_no = modbus[i]["cards"]
        card = {"card": cards[x:x + card_no]}
        modbus[i]["cards"] = card
        md_list.append(modbus[i])
        x += card_no

    if len(md_list) == 1:
        md_dict["modbus_device"] = md_list[0]
    else:
        md_dict["modbus_device"] = md_list
    modbus_devices.append(md_dict)

    return modbus_devices


def settings2db(xml_data, controller_id, time):
    if ControllerSystem.objects.filter(controller_id=controller_id).exists():
        object = ControllerSystem.objects.get(controller_id=controller_id)
        settings_str = object.system_settings
        settings_dict = json.loads(settings_str)
    else:
        object = ControllerSystem(controller_id=controller_id)
        settings_dict = {}

    for element in xml_data.iter("system_sett"):
        for x in element:
            if validate_text(x.text):
                settings_dict[x.tag] = str(x.text)

    object.system_settings = json.dumps(settings_dict)
    object.time = time
    object.save()


def circ2db(xml_data, controller_id, time):
    sync = ControllerHeatcircSync.objects.get(controller_id=controller_id)
    sync.time = time
    sync.save()

    for element in xml_data.iter("heatcirc_sett"):
        item_id = int(element.find("id").text)
        objects = ControllerHeatcirc.objects.raw(
            'SELECT * FROM controller_heatcirc WHERE controller_id = %s AND item_id = %s', [controller_id, item_id])
        if len(list(objects)) > 0:
            object = objects[0]

            settings_str = object.settings
            settings_dict = json.loads(settings_str)
            for x in element:
                if validate_text(x.text):
                    settings_dict[x.tag] = str(x.text)

            settings_str = json.dumps(settings_dict)
            with connection.cursor() as c:
                c.execute(
                    "UPDATE controller_heatcirc SET settings = %s, time = %s WHERE controller_id = %s AND item_id = %s",
                    [settings_str, time, controller_id, item_id])

        else:
            settings_dict = {}
            for x in element:
                if validate_text(x.text):
                    settings_dict[x.tag] = str(x.text)

            settings = json.dumps(settings_dict)
            with connection.cursor() as c:
                c.execute(
                    "INSERT INTO controller_heatcirc (controller_id, item_id, settings, time) VALUES (%s,%s,%s,%s)",
                    [controller_id, item_id, settings, time])


def gen2db(xml_data, controller_id, time):
    sync = ControllerHeatgenSync.objects.get(controller_id=controller_id)
    sync.time = time
    sync.save()

    for element in xml_data.iter("heatgen_sett"):
        item_id = int(element.find("id").text)
        objects = ControllerHeatgen.objects.raw(
            'SELECT * FROM controller_heatgen WHERE controller_id = %s AND item_id = %s', [controller_id, item_id])
        if len(list(objects)) > 0:
            object = objects[0]

            settings_str = object.settings
            settings_dict = json.loads(settings_str)
            for x in element:
                if validate_text(x.text):
                    settings_dict[x.tag] = str(x.text)

            settings_str = json.dumps(settings_dict)
            with connection.cursor() as c:
                c.execute(
                    "UPDATE controller_heatgen SET settings = %s, time = %s WHERE controller_id = %s AND item_id = %s",
                    [settings_str, time, controller_id, item_id])

        else:
            settings_dict = {}
            for x in element:
                if validate_text(x.text):
                    settings_dict[x.tag] = str(x.text)

            settings = json.dumps(settings_dict)
            with connection.cursor() as c:
                c.execute(
                    "INSERT INTO controller_heatgen (controller_id, item_id, settings, time) VALUES (%s,%s,%s,%s)",
                    [controller_id, item_id, settings, time])


def profiles2db(xml_data, controller_id, time):
    sync = ControllerProfilesSync.objects.get(controller_id=controller_id)
    sync.time = time
    sync.save()

    try:
        active_profile = int(xml_data.find(".//active_profile_id").text)
    except (AttributeError, TypeError):
        active_profile = ""

    if active_profile:
        controller = Controller.objects.get(controller_id=controller_id)
        controller.active_profile_id = int(active_profile)
        controller.save()

    for element in xml_data.iter("profile"):
        try:
            item_id = element.find("id").text
        except (AttributeError, TypeError):
            continue
        try:
            name = element.find("name").text
        except (AttributeError, TypeError):
            name = ""

        if ControllerProfiles.objects.filter(controller_id=controller_id, item_id=item_id).exists():
            profile = ControllerProfiles.objects.get(controller_id=controller_id, item_id=item_id)
            profile.item_id = item_id
            if name:
                profile.name = name
            profile.time = time
            profile.save()
        else:
            profile = ControllerProfiles(controller_id=controller_id, item_id=item_id, profile_name=name, time=time)
            profile.save()


def user_setts2db(xml_data, controller_id, time, type):
    sync = ControllerUserSettingsSync.objects.get(controller_id=controller_id)
    sync.time = time
    sync.save()

    if type == "uSett_hc":
        root_name = "hc_user_sett"
    elif type == "uSett_dhw":
        root_name = "dhw_user_sett"

    for element in xml_data.iter(root_name):
        try:
            item_id = int(element.find(".//id").text)
        except (AttributeError, TypeError):
            continue

        if ControllerUserSettings.objects.filter(type=type, controller_id=controller_id, item_id=item_id).exists():
            object = ControllerUserSettings.objects.get(type=type, controller_id=controller_id, item_id=item_id)
        else:
            object = ControllerUserSettings(type=type, controller_id=controller_id, item_id=item_id)

        try:
            name = str(element.find(".//name").text)
        except (AttributeError, TypeError):
            name = ""
        try:
            mode = str(element.find(".//mode").text)
        except (AttributeError, TypeError):
            mode = ""
        try:
            outcmp_coeff = float(element.find(".//outcmp_coeff").text)
        except (AttributeError, TypeError):
            outcmp_coeff = ""
        try:
            party_tset = float(element.find(".//party_tset").text)
        except (AttributeError, TypeError):
            party_tset = ""
        try:
            enabled = int(element.find(".//enabled").text)
        except (AttributeError, TypeError):
            enabled = ""
        try:
            eco_tset = float(element.find(".//eco_tset").text)
        except (AttributeError, TypeError):
            eco_tset = ""
        try:
            outcmp_limit = float(element.find(".//outcmp_limit").text)
        except (AttributeError, TypeError):
            outcmp_limit = ""
        try:
            eco_cool_tset = float(element.find(".//eco_cool_tset").text)
        except (AttributeError, TypeError):
            eco_cool_tset = ""
        try:
            party_cool_tset = float(element.find(".//party_cool_tset").text)
        except (AttributeError, TypeError):
            party_cool_tset = ""
        try:
            freeze_tset = float(element.find(".//freeze_tset").text)
        except (AttributeError, TypeError):
            freeze_tset = ""
        try:
            hc_mode = str(element.find(".//hc_mode").text)
        except (AttributeError, TypeError):
            hc_mode = ""
        try:
            outcmp_hrs = float(element.find(".//outcmp_hrs").text)
        except (AttributeError, TypeError):
            outcmp_hrs = ""
        try:
            freeze_prot = int(element.find(".//freeze_prot").text)
        except (AttributeError, TypeError):
            freeze_prot = ""
        try:
            outcmp = int(element.find(".//outcmp").text)
        except (AttributeError, TypeError):
            outcmp = ""

        if name:
            object.name = name
        if mode:
            object.mode = mode
        if outcmp_coeff:
            object.outcmp_coeff = outcmp_coeff
        if party_tset:
            object.party_tset = party_tset
        if enabled:
            object.enabled = enabled
        if eco_tset:
            object.eco_tset = eco_tset
        if outcmp_limit:
            object.outcmp_limit = outcmp_limit
        if eco_cool_tset:
            object.eco_cool_tset = eco_cool_tset
        if party_cool_tset:
            object.party_cool_tset = party_cool_tset
        if freeze_tset:
            object.freeze_tset = freeze_tset
        if hc_mode:
            object.hc_mode = hc_mode
        if outcmp_hrs:
            object.outcmp_hrs = outcmp_hrs
        if freeze_prot:
            object.freeze_prot = freeze_prot
        if outcmp:
            object.outcmp = outcmp
        if time:
            object.time = time

        object.save()


def schedules2db(xml_data, controller_id, time, type):
    sync = ControllerScheduleSync.objects.get(controller_id=controller_id)
    sync.time = time
    sync.save()

    if type == "sch_hc":
        root_name = "hc_user_schedule"
    elif type == "sch_dhw":
        root_name = "dhw_user_schedule"

    for element in xml_data.iter(root_name):
        try:
            item_id = int(element.find("id").text)
        except (AttributeError, TypeError):
            continue

        try:
            profile_id = int(element.find("profile_id").text)
        except (AttributeError, TypeError):
            continue

        try:
            mode = str(element.find("type").text)
        except (AttributeError, TypeError):
            continue

        if ControllerSchedule.objects.filter(controller_id=controller_id, profile_id=profile_id, item_id=item_id, type=type, mode=mode).exists():
            object = ControllerSchedule.objects.get(controller_id=controller_id, profile_id=profile_id, item_id=item_id, type=type, mode=mode)
        else:
            object = ControllerSchedule(controller_id=controller_id, profile_id=profile_id, item_id=item_id, type=type, mode=mode)

        object.time = time
        object.save()

        schedule_id = object.id

        for item in element.iter("item"):
            try:
                day = int(item.find("d").text)
            except (AttributeError, TypeError):
                day = 0
            try:
                hour = int(item.find("h").text)
            except (AttributeError, TypeError):
                hour = 0
            try:
                min = int(item.find("m").text)
            except (AttributeError, TypeError):
                min = 0
            try:
                set_temp = float(item.find("tset").text)
            except (AttributeError, TypeError):
                set_temp = ""
            try:
                enabled = int(item.find("enabled").text)
            except (AttributeError, TypeError):
                enabled = 0

            objects = ControllerScheduleInterval.objects.raw(
                'SELECT * FROM controller_schedule_interval WHERE schedule_id = %s AND day = %s and hour = %s and min = %s',
                [schedule_id, day, hour, min])

            if len(list(objects)) > 0:
                with connection.cursor() as c:
                    c.execute(
                        'UPDATE controller_schedule_interval SET enabled = %s, set_temp = %s WHERE schedule_id = %s AND day = %s and hour = %s and min = %s',
                        [enabled, set_temp, schedule_id, day, hour, min])
            else:
                with connection.cursor() as c:
                    c.execute(
                        "INSERT INTO controller_schedule_interval (schedule_id, enabled, day, hour, min, set_temp) VALUES (%s,%s,%s,%s,%s,%s)",
                        [schedule_id, enabled, day, hour, min, set_temp])


def files2db(xml_data, controller_id, time):

    if ControllerIniSync.objects.filter(controller_id=controller_id).exists():
        sync = ControllerIniSync.objects.get(controller_id=controller_id)
        sync.time = time
        sync.save()
    else:
        ControllerIniSync(controller_id=controller_id, user_id=0, force_update=0, time=time).save()

    ini_types = ControllerIniType.objects.all()
    types = []
    for type in ini_types:
        types.append(type.name)

    for file in xml_data.iter("file"):
        name = str(file.find("name").text)
        if name in types:
            try:
                encoded = str(file.find("encoded").text)
            except (AttributeError, TypeError):
                encoded = ""
            if encoded:
                content = ""
                for part in file.iter("part"):
                    content += str(part.text)
            else:
                content_str = str(file.find("content").text)
                content = base64.b64encode(content_str.encode('utf-8'))

            if ControllerIni.objects.filter(controller_id=controller_id, name=name).exists():
                object = ControllerIni.objects.get(controller_id=controller_id, name=name)
                object.content = content
                object.time = time
                object.save()
            else:
                ControllerIni(controller_id=controller_id, name=name, content=content, user_id=0, time=time).save()

        else:
            continue


def meters2json(xml_data):
    list = []
    for meter in xml_data.iter("meters"):
        for child in meter:
            dict = {}
            for element in child:
                if len(element) > 0 and element.tag == "data":
                    params = {}
                    for parm in element:
                        name = str(parm.find('.//name').text)
                        value = float(parm.find('.//value').text)
                        params[name] = value
                    dict[element.tag] = params
                elif validate_text(element.text):
                    try:
                        dict[element.tag] = int(element.text)
                    except ValueError:
                        try:
                            dict[element.tag] = float(element.text)
                        except ValueError:
                            dict[element.tag] = str(element.text)
            list.append(dict)
    return list


def meters2db(controller_id, meters_list):
    for meter in meters_list:
        usr_id = int(meter["usr_id"])
        data_time = int(meter["data_time"])

        object = ControllerLogsHeatmeters.objects.raw(
            'SELECT * FROM controller_logs_heatmeters WHERE controller_id = %s AND usr_id = %s ORDER BY time DESC limit 1',
            [int(controller_id), usr_id])

        try:
            latest_log_time = int(object[0].time) + 60
        except (AttributeError, IndexError):
            latest_log_time = 0

        if data_time > latest_log_time:
            data = meter["data"]

            try:
                energy = data["energy"]
            except KeyError:
                energy = 0.0
            try:
                power = data["power"]
            except KeyError:
                power = 0.0
            try:
                volume = data["volume"]
            except KeyError:
                volume = 0.0
            try:
                volume_flow = data["volume_flow"]
            except KeyError:
                volume_flow = 0.0
            try:
                flow_temp = data["flow_temp"]
            except KeyError:
                flow_temp = 0.0
            try:
                return_temp = data["return_temp"]
            except KeyError:
                return_temp = 0.0
            try:
                temp_diff = data["temp_diff"]
            except KeyError:
                temp_diff = 0.0

            with connection.cursor() as c:
                c.execute(
                    "INSERT INTO controller_logs_heatmeters (controller_id, time, usr_id, energy, power, volume, volume_flow, flow_temp, return_temp, temp_diff) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                    [int(controller_id), data_time, usr_id, energy, power, volume, volume_flow, flow_temp, return_temp, temp_diff])
