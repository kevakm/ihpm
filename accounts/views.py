import hashlib
import json
import uuid

import time

import datetime

from django.contrib import messages
from django.contrib.auth import login
from django.db import connection
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import TemplateView

from api.models import Controller, AppUserRoleData, AppUserData, AppUserRoles, AppUserControllerAccess, \
    AppUserDataExtra, \
    Countries, Cities


class RegistrationView(TemplateView):
    template_name = 'registration.html'

    def __init__(self):
        self.username = ""
        self.name = ""
        self.last_name = ""
        self.email = ""
        self.salt = ""
        self.password = ""
        self.role_id = ""
        self.controllers = ""
        self.date_created = int(time.time())
        self.date_modified = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.user_id = ""
        self.country_id = ""
        self.state = ""
        self.zip = ""
        self.city = ""
        self.language = ""

    def hash_password(self, password):
        self.salt = uuid.uuid4().hex
        self.password = hashlib.sha256(password.encode('utf-8') + self.salt.encode('utf-8')).hexdigest()

    def register_user(self):
        if AppUserData.objects.filter(username=self.username).exists():
            user = AppUserData.objects.get(username=self.username)
            user.password = self.password
            user.name = self.name
            user.last_name = self.last_name
            user.email = self.email
            user.salt = self.salt
            user.date_modified = self.date_modified
            user.save()
        else:
            AppUserData(username=self.username, password=self.password, name=self.name, last_name=self.last_name,
                        email=self.email, salt=self.salt, date_created=self.date_created,
                        date_modified=self.date_modified).save()

    def add_extra_data(self):
        if AppUserDataExtra.objects.filter(user_id=self.user_id).exists():
            extra = AppUserDataExtra.objects.get(user_id=self.user_id)
            extra.state = self.state
            extra.zip = self.zip
            extra.city = self.city
            extra.country_id = self.country_id
            extra.language = self.language
            extra.date_modified = self.date_modified
            extra.save()
        else:
            AppUserDataExtra(user_id=self.user_id, state=self.state, zip=self.zip,
                             city=self.city, country_id=self.country_id, language=self.language,
                             date_created=self.date_created, date_modified=self.date_modified).save()

    def add_role(self):
        if AppUserRoles.objects.filter(user_id=self.user_id).exists():
            role = AppUserRoles.objects.get(user_id=self.user_id)
            role.role_id = self.role_id
            role.date_modified = self.date_modified
            role.save()
        else:
            AppUserRoles(user_id=self.user_id, role_id=self.role_id, date_created=self.date_created,
                         date_modified=self.date_modified).save()

    def add_controllers(self):
        all_cont = [access.controller_id for access in AppUserControllerAccess.objects.filter(user_id=self.user_id)]
        for controller in self.controllers:

            if AppUserControllerAccess.objects.filter(user_id=self.user_id, controller_id=int(controller)).exists():
                controller_access = AppUserControllerAccess.objects.get(user_id=self.user_id,
                                                                        controller_id=int(controller))
                controller_access.date_modified = self.date_modified
                controller_access.save()
                all_cont.remove(int(controller))
            else:
                with connection.cursor() as c:
                    c.execute(
                        "INSERT INTO app_user_controller_access (user_id, controller_id, date_created, date_modified) VALUES (%s,%s,%s,%s)",
                        [self.user_id, int(controller), self.date_created, self.date_modified])

        for controller in all_cont:
            if AppUserControllerAccess.objects.filter(user_id=self.user_id, controller_id=int(controller)).exists():
                AppUserControllerAccess.objects.get(user_id=self.user_id, controller_id=int(controller)).delete()

    def get_context_data(self, **kwargs):
        context = super(RegistrationView, self).get_context_data(**kwargs)
        context['users'] = AppUserData.objects.all()
        context['roles'] = AppUserRoleData.objects.all()
        context['controllers'] = Controller.objects.all()
        return context

    def get(self, *args):
        role = self.request.session.get('login_role', '')
        if self.request.session._session_key is None or role != 5:
            return HttpResponseRedirect(reverse_lazy('accounts:login') + "?next=registration")
        else:
            return super(RegistrationView, self).get(*args)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            get_user_data = request.POST.get('get_user_data', '')
            delete_user_id = request.POST.get('delete_user_id', '')
            registration = request.POST.get('registration', '')

            if get_user_data:
                user = AppUserData.objects.get(user_id=get_user_data)
                self.user_id = user.user_id
                extra = AppUserDataExtra.objects.get(user_id=self.user_id)
                role = AppUserRoles.objects.get(user_id=self.user_id)
                controllers = [int(controller.controller_id) for controller in
                               AppUserControllerAccess.objects.filter(user_id=self.user_id)]
                py_dict = {"username": user.username, "name": user.name, "last_name": user.last_name,
                           "email": user.email, "state": extra.state, "zip": extra.zip,
                           "city": extra.city, "country_id": extra.country_id, "language": extra.language,
                           "role": role.role_id, "controllers": controllers}

                return JsonResponse(py_dict)

            if delete_user_id:
                AppUserData.objects.filter(user_id=int(delete_user_id)).delete()
                AppUserDataExtra.objects.filter(user_id=int(delete_user_id)).delete()
                AppUserRoles.objects.filter(user_id=int(delete_user_id)).delete()
                AppUserControllerAccess.objects.filter(user_id=int(delete_user_id)).delete()
                return JsonResponse("", safe=False)

            if registration:
                py_dict = json.loads(registration)

                self.username = py_dict['username']
                self.name = py_dict['name']
                self.last_name = py_dict['last_name']
                self.email = py_dict['email']
                self.hash_password(py_dict['password'])
                self.role_id = py_dict['role']
                self.controllers = py_dict['controllers']
                self.date_created = int(time.time())
                self.date_modified = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                self.register_user()
                self.country_id = py_dict['country_id']
                self.state = py_dict['state']
                self.zip = py_dict['zip']
                self.city = py_dict['city']
                self.language = py_dict['language']

                user = AppUserData.objects.get(username=self.username)
                self.user_id = user.user_id

                self.add_extra_data()
                self.add_role()
                self.add_controllers()

                return JsonResponse({'message': "Success"})


class LoginView(TemplateView):
    template_name = 'login.html'

    def __init__(self):
        self.username = ""
        self.password = ""
        self.salt = ""

    def verify_password(self, hashed_password):
        re_hashed = hashlib.sha256(self.password.encode('utf-8') + self.salt.encode('utf-8')).hexdigest()
        return re_hashed == hashed_password

    def get(self, *args):
        role = self.request.session.get('login_role', '')
        if self.request.session._session_key is None or role != 5:
            return super(LoginView, self).get(*args)
        else:
            return HttpResponseRedirect(reverse_lazy('accounts:registration'))

    def post(self, request, *args, **kwargs):
        self.username = request.POST.get('username', '')
        self.password = request.POST.get('password', '')

        if self.password == "" or self.username == "":
            messages.error(request, 'Please fill in both fields.')
            return super(LoginView, self).get(request, *args, **kwargs)

        try:
            user_profile = AppUserData.objects.get(username=self.username)
            hashed_password = user_profile.password
            self.salt = user_profile.salt
            if self.verify_password(hashed_password):
                request.session['login_id'] = user_profile.user_id
                role = AppUserRoles.objects.get(user_id=user_profile.user_id).role_id
                request.session['login_role'] = role
                next = request.GET.get('next', '')
                if next == "":
                    next = "registration"
                return HttpResponseRedirect(reverse_lazy('accounts:' + next + ''))
            else:
                messages.error(request, 'Password and username does not match!')
                return super(LoginView, self).get(request, *args, **kwargs)
        except AppUserData.DoesNotExist:
            messages.error(request, 'That username does not exist!')
            return super(LoginView, self).get(request, *args, **kwargs)

        return super(LoginView, self).get(request, *args, **kwargs)


class ControllersView(TemplateView):
    template_name = 'controllers.html'

    def get_context_data(self, **kwargs):
        context = super(ControllersView, self).get_context_data(**kwargs)
        context['controllers'] = Controller.objects.all().order_by('serial_no')
        context['countries'] = Countries.objects.all()
        return context

    def get(self, *args):
        role = self.request.session.get('login_role', '')
        if self.request.session._session_key is None or role != 5:
            return HttpResponseRedirect(reverse_lazy('accounts:login') + "?next=controllers")
        else:
            return super(ControllersView, self).get(*args)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            get_controller_data = request.POST.get('get_controller_data', '')
            registration = request.POST.get('registration', '')
            country_id = request.POST.get('country_id', '')
            delete_controller_id = request.POST.get('delete_controller_id', '')

            if country_id:
                cities = Cities.objects.filter(country_id=int(country_id))
                cities_list = []
                for city in cities:
                    cities_list.append({"id": city.id, "name": city.name})

                return JsonResponse({"cities": cities_list})

            if delete_controller_id:
                controller = Controller.objects.get(controller_id=delete_controller_id)
                controller.delete()

                return JsonResponse({'message': "Success"})

            if get_controller_data:
                controller = Controller.objects.get(controller_id=get_controller_data)
                py_dict = {"serial_no": controller.serial_no, "controller_name": controller.name,
                           "country_id": controller.country_id, "city_id": controller.city_id,
                           "status": controller.status, "auth_wildcard": controller.auth_wildcard,
                           "resque": controller.resque}
                return JsonResponse(py_dict)

            if registration:
                py_dict = json.loads(registration)
                timestamp = int(time.time())
                date_created = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                controller_sn = int(py_dict['serial_no'])

                if Controller.objects.filter(serial_no=controller_sn).exists():
                    controller = Controller.objects.get(serial_no=controller_sn)
                    controller.name = py_dict['name']
                    controller.status = int(py_dict['status'])
                    controller.country_id = int(py_dict['country_id'])
                    controller.city_id = int(py_dict['city_id'])
                    controller.last_change = timestamp
                    controller.auth_wildcard = int(py_dict['auth_wildcard'])
                    controller.resque = int(py_dict['resque'])
                    controller.save()
                else:
                    Controller(serial_no=controller_sn, name=py_dict['name'], status=int(py_dict['status']),
                               country_id=int(py_dict['country_id']), city_id=int(py_dict['city_id']),
                               last_change=timestamp, date_created=timestamp, timestamp=date_created,
                               auth_access_token="",
                               auth_wildcard=int(py_dict['auth_wildcard']), resque=int(py_dict['resque'])).save()

                return JsonResponse({'message': "Success"})
