from django.conf.urls import url

from accounts.views import RegistrationView, LoginView, ControllersView

urlpatterns = [
    url(r'^registration/$', RegistrationView.as_view(), name='registration'),
    url(r'^$', LoginView.as_view(), name='login'),
    url(r'^controllers/$', ControllersView.as_view(), name='controllers'),
]
