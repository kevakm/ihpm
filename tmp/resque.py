#!/usr/bin/python3

import requests
import xml.etree.ElementTree as ET
import os
from struct import *


def get_serial():
    try:
        f = open("/pipc/sn.bin", "rb")
        sn = f.read(8)
        un = unpack('II', sn)
        f.close()
        return un[1]
    except Exception:
        return 0


hostname = "ihpmdev.i13tech.com"
controller_sn = get_serial()
# controller_sn = 13  # DEBUG ONLY !!!!!!
if controller_sn < 1:
    print("Serial number error")
    exit(1)

resque_url = "https://{}/rest/bin/resque".format(hostname)
resque_body = """<?xml version="1.0" encoding="UTF-8"?><xml><request><controller>{}</controller></request></xml>""".format(controller_sn)
key_file_name = "/pipc/controller{}.pem".format(controller_sn)
cmd = "ssh -o StrictHostKeyChecking=no -i {} -f -N -T -R 22222:127.0.0.1:43204 admin@{}".format(key_file_name, hostname)
print("Connecting to " + resque_url)

r = requests.post(resque_url, data=resque_body, verify=False, timeout=3)

if r.status_code != 200:
    print("Error status code: " + str(r.status_code))
    exit(1)

try:
    xml_data = ET.fromstring(r.text)
    private_key = xml_data.find('.//private_key')
except Exception:
    print("Error parsing xml")
    exit(1)

os.system("mkdir -p /pipc")


with open(key_file_name, "w") as f:
    f.write(private_key.text)

os.system("chmod 600 " + key_file_name)
os.system(cmd)
